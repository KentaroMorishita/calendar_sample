/**
 * カレンダー表示用クラス
 */
class Calendar {

	/**
	 * コンストラクタ
	 * 第２引数をtrueにすると日曜日スタートで表示する
	 * @param {Moment} moment
	 * @param {boolean} is_sunday_start
	 */
	constructor(moment, is_sunday_start = false) {
		this.calendar = document.querySelector('.calendar');
		this.year_select = document.querySelector('.year');
		this.month_select = document.querySelector('.month');
		this.prev = document.querySelector('.prev');
		this.next = document.querySelector('.next');
		this.today = moment().startOf('day');
		this.date = moment().date(1);
		this.is_sunday_start = is_sunday_start;
	}

	/**
	 * イベントのバインドとカレンダーの初期表示
	 * @returns {void}
	 */
	init() {
		this.onChangeYear();
		this.onChangeMonth();
		this.onClickPrev();
		this.onClickNext();
		this.generateCalendar();
	}

	/**
	 * 年度選択イベント処理
	 * @returns {void}
	 */
	onChangeYear() {
		this.year_select.addEventListener('change', e => {
			let val = e.currentTarget.value;
			this.date.year(val);
			this.generateCalendar();
		}, false);
	}

	/**
	 * 月選択イベント処理
	 * @returns {void}
	 */
	onChangeMonth() {
		this.month_select.addEventListener('change', e => {
			let val = e.currentTarget.value - 1;
			this.date.month(val);
			this.generateCalendar();
		}, false);
	}

	/**
	 * 先月表示イベント処理
	 * @returns {void}
	 */
	onClickPrev() {
		this.prev.addEventListener('click', e => {
			e.preventDefault();
			this.date.subtract(1, 'months');
			this.generateCalendar();
		}, false);
	}

	/**
	 * 来月表示イベント処理
	 * @returns {void}
	 */
	onClickNext() {
		this.next.addEventListener('click', e => {
			e.preventDefault();
			this.date.add(1, 'months');
			this.generateCalendar();
		}, false);
	}

	/**
	 * カレンダーの表示
	 * @returns {void}
	 */
	generateCalendar() {
		this.setYearSelectOptions();
		this.setMonthSelectOptions();

		const STAURDAY = 6;
		const SUNDAY = 0;
		const endOfMonth = this.date.clone().endOf('month');

		let dates = [];
		for (let d of this.range(1, endOfMonth.date())) {
			dates.push(this.date.clone().add(d - 1, 'day'));
		}

		let calendar_body = '';
		let calendar_table = `
		<table class="table table-bordered">
			<thead>
				<tr>
					${ this.is_sunday_start?'<th class="sunday">日</th>': ''}
					<th>月</th>
					<th>火</th>
					<th>水</th>
					<th>木</th>
					<th>金</th>
					<th class="saturday">土</th>
					${ this.is_sunday_start?'':'<th class="sunday">日</th>'}
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>`;

		this.paddingPrev(dates);
		this.paddingNext(dates, endOfMonth);

		this.datesChunck(dates).forEach(chunk => {
			calendar_body += '<tr>';
			chunk.forEach(v => {
				let klass = '';
				switch (v.day()) {
					case STAURDAY:
						klass = 'saturday';
						break;
					case SUNDAY:
						klass = 'sunday';
						break;
				}
				if (v.month() != this.date.month()) {
					klass = 'gray-day';
				}

				if (this.today.isSame(v.format('YYYY-MM-DD'))) {
					klass += 'today';
				}

				calendar_body += `<td class="${klass}">${v.date()}</td>`;
			});
			calendar_body += '</tr>';
		});

		this.calendar.innerHTML = calendar_table;
		this.calendar.querySelector('tbody').innerHTML = calendar_body;
	}

	/**
	 * rangeジェネレータ
	 * @param {int} from
	 * @param {int} to
	 * @returns {Generator}
	 */
	* range(from, to) {
		while (from <= to) {
			yield from++;
		}
	}

	/**
	 * 年度のセレクトオプションのセット
	 * @returns {void}
	 */
	setYearSelectOptions() {
		let years = [];
		let range = this.range(this.date.year() - 2, this.date.year() + 2);
		for (let year of range) {
			let selected = year == this.date.year()?'selected': '';
			years.push(`<option value="${year}" ${selected}>${year}</option>`);
		}
		this.year_select.innerHTML = years.join("\n");
	}

	/**
	 * 月のセレクトオプションのセット
	 * @returns {void}
	 */
	setMonthSelectOptions() {
		let months = [];
		for (let month of this.range(1, 12)) {
			let selected = month == this.date.month() + 1?'selected': '';
			months.push(`<option value="${month}" ${selected}>${month}</option>`);
		}
		this.month_select.innerHTML = months.join("\n");
	}

	/**
	 * カレンダーの先月の日付を埋める
	 * @param {Moment} dates
	 * @returns {Array}
	 */
	paddingPrev(dates) {
		let startDay = dates[0].day() !== 0?dates[0].day(): 7;
		if (this.is_sunday_start) {
			startDay = dates[0].day();
		}
		let date = this.date.clone();
		let stop = this.is_sunday_start?0:1;
		for (let i = startDay; i > stop; i--) {
			let paddingDate = date.subtract(1, 'day').clone();
			dates.unshift(paddingDate);
		}
		return dates;
	}

	/**
	 * カレンダーの来月の日付を埋める
	 * @param {Array} dates
	 * @param {int} endOfMonth
	 * @returns {Array}
	 */
	paddingNext(dates, endOfMonth) {
		let endDay = endOfMonth.day();
		let date = endOfMonth.clone();
		if (!(this.is_sunday_start || endDay)) {
			return dates;
		}
		let stop = this.is_sunday_start?6: 7;
		for (let i = endDay; i < stop; i++) {
			let paddingDate = date.add(1, 'day').clone();
			dates.push(paddingDate);
		}
		return dates;
	}

	/**
	 * 日付の配列を週ごとの固まりの配列に分割する
	 * @param {Array} dates
	 * @returns {Array}
	 */
	datesChunck(dates) {
		let n = 7, res = [];
		for (let chunk of this.range(0, Math.round(dates.length / n) - 1)) {
			res.push(dates.slice(chunk * n, chunk * n + n));
		}
		return res;
	}

}